# jsSHA

Provides Drupal integration with jsSHA, a JavaScript implementation of the
complete Secure Hash Standard family as well as HMAC.

## Installation

This module requires the jsSHA library from <https://github.com/Caligatio/jsSHA>.

1. Download the jsSHA plugin from <https://github.com/Caligatio/jsSHA>
2. Place the plugin folder in the root libraries folder (/libraries/jsSHA).
3. Enable jsSHA in the Drupal admin.

## Basic Usage

There is no configuration. See <https://github.com/Caligatio/jsSHA> for details.

## Advanced Usage

To install the jsSHA library with Composer, make the below additions to your
project's root composer.json file.

Add a `repositories` entry for the `jsSHA` library.

```json
"repositories": {
  "jsSHA": {
    "type": "package",
    "package": {
      "name": "js_sha/js_sha",
      "version": "v2.4.2",
      "type": "drupal-library",
      "extra": {
        "installer-name": "jsSHA"
      },
      "dist": {
        "url": "https://github.com/Caligatio/jsSHA/archive/v2.4.2.zip",
        "type": "zip"
      }
    }
  }
}
```

Require the `js_sha` library for your project.

```json
"require": {
  "composer/installers": "^2.3",
  "js_sha/js_sha": "v2.4.2"
}
```
